import * as React from 'react';
import axios from 'axios';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import Avatar from '@mui/material/Avatar';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import IconButton from '@mui/material/IconButton';
import SettingsIcon from '@mui/icons-material/Settings';
import PopoverComponent from '../common/PopoverComponent';
import DialogComponent from '../common/DialogComponent';

function UserDetailsList(props) {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openDialog, setOpenDialog] = React.useState(false);

  const onClickMoreHandler = (event, item) => {
    setAnchorEl(event.currentTarget);
    props.setItem(item);
  };

  const handleEditClose = () => {
    setAnchorEl(null);
  };

  const handleRemoveClose = () => {
    setOpenDialog(false)
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const columns = [
    {
      field: 'name', headerName: 'Name', flex: 1,
      renderCell: (params) => {
        return (
          <><Avatar src={params.row.image_url} />{params.row.first_name} {params.row.last_name} </>
        )
      }
    },
    { field: 'id', headerName: 'ID', flex: 1, },
    { field: 'email', headerName: 'Email', flex: 1, },
    { field: 'description', headerName: 'Description', flex: 1, },
    {
      field: 'settings', headerName: <IconButton> <SettingsIcon /></IconButton>,
      renderCell: (params) => <IconButton onClick={(event) => onClickMoreHandler(event, params.row)}> <MoreVertIcon /></IconButton>,
      sortable: false, filterable: false
    }
  ];

  const onClickEditHandler = () => {
    setAnchorEl(null)
    props.setButtonText('Edit profile');
    props.setShowCreateProfile(true)
  }

  const onClickRemoveHandler = () => {
    setAnchorEl(null)
    setOpenDialog(true);
  }

  const handleDelete = () => {
    setOpenDialog(false);
    const header =
    {
      'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVfbmFtZSI6IkFpdGV5IiwiaXNfY2FuZGlkYXRlIjp0cnVlLCJpYXQiOjE2Nzg5MDUxMTcsImV4cCI6MTY3OTQyMzUxN30.7-kBNeJpJnH1XjIa9vQsd1RZgyx5oaLm3_Qva8bT5W4'

    }
    const body = {
      'query': 'mutation DeleteProfile($deleteProfileId: String!) {\n  deleteProfile(id: $deleteProfileId)\n}',
      'variables': { 'deleteProfileId': props.item.id }
    }
    axios({
      method: 'POST',
      url: 'https://api.poc.graphql.dev.vnplatform.com/graphql',
      headers: header,
      data: body
    })
      .then(response => {
        const updatedData = props.data.filter(item => item.id !== response.data.data.deleteProfile.id);
        props.setUserProfiles(updatedData);
      })
      .catch(error => console.log(error))
  }

  return (
    <>
      <Box sx={{ height: '70vh', width: '83%', marginLeft: '7%', marginTop: '3%' }}>

        <DataGrid
          rows={props.data}
          columns={columns}
          initialState={{
            ...props.data.initialState,
            pagination: { paginationModel: { pageSize: 5 } },
          }}
          rowHeight={70}
          pageSizeOptions={[5, 10, 25]}
          experimentalFeatures={{ newEditingApi: true }}
          disableColumnMenu
          disableRowSelectionOnClick
        />
      </Box>
      <PopoverComponent
        id={id}
        open={open}
        anchorEl={anchorEl}
        handleClose={handleEditClose}
        onClickEditHandler={onClickEditHandler}
        onClickRemoveHandler={onClickRemoveHandler}
      />
      <DialogComponent
        open={openDialog}
        handleClose={handleRemoveClose}
        handleDelete={handleDelete}
      />
    </>
  )
}

export default UserDetailsList;