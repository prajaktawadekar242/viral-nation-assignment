import React from 'react';
import axios from 'axios';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Typography from '@mui/material/Typography';
import PopoverComponent from '../common/PopoverComponent';
import DialogComponent from '../common/DialogComponent';

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    //flexGrow: 1,
    padding: theme.spacing(1)
  }
}));

function UserDetailsCard(props) {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openDialog, setOpenDialog] = React.useState(false);
  const classes = useStyles();

  const onClickMoreHandler = (event, item) => {
    setAnchorEl(event.currentTarget);
    props.setItem(item);
  };

  const handleEditClose = () => {
    setAnchorEl(null);
  };

  const handleRemoveClose = () => {
    setOpenDialog(false)
  };

  const handleDelete = () => {
    setOpenDialog(false);
    const header =
    {
      'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVfbmFtZSI6IkFpdGV5IiwiaXNfY2FuZGlkYXRlIjp0cnVlLCJpYXQiOjE2Nzg5MDUxMTcsImV4cCI6MTY3OTQyMzUxN30.7-kBNeJpJnH1XjIa9vQsd1RZgyx5oaLm3_Qva8bT5W4'

    }
    const body = {
      'query': 'mutation DeleteProfile($deleteProfileId: String!) {\n  deleteProfile(id: $deleteProfileId)\n}',
      'variables': { 'deleteProfileId': props.item.id }
    }
    axios({
      method: 'POST',
      url: 'https://api.poc.graphql.dev.vnplatform.com/graphql',
      headers: header,
      data: body
    })
      .then(response => {
        const updatedData = props.data.filter(item => item.id !== response.data.data.deleteProfile.id);
        props.setUserProfiles(updatedData);
      })
      .catch(error => console.log(error))
  }

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const onClickEditHandler = () => {
    setAnchorEl(null)
    props.setButtonText('Edit profile');
    props.setShowCreateProfile(true)
  }

  const onClickRemoveHandler = () => {
    setAnchorEl(null)
    setOpenDialog(true);
  }

  return (
    <div className={classes.root}>

      <Card sx={{
        maxWidth: 280, maxHeight: 180, marginLeft: '1%', marginBottom: '1%',
        width: 280, height: 180
      }}>
        <CardHeader
          avatar={
            <Avatar src={props.cardData.image_url}>
            </Avatar>
          }
          action={
            <IconButton aria-label='settings' onClick={(event) => onClickMoreHandler(event, props.cardData)}>
              <MoreVertIcon />
            </IconButton>
          }
          title={props.cardData.first_name + ' ' + props.cardData.last_name}
          subheader={props.cardData.email}
        />
        <CardContent sx={{ paddingTop: '0px' }}>
          <Typography variant='body2' color='text.secondary'>
            {props.cardData.description}
          </Typography>
        </CardContent>
      </Card>

      <PopoverComponent
        id={id}
        open={open}
        anchorEl={anchorEl}
        handleClose={handleEditClose}
        onClickEditHandler={onClickEditHandler}
        onClickRemoveHandler={onClickRemoveHandler}
      />
      <DialogComponent
        open={openDialog}
        handleClose={handleRemoveClose}
        handleDelete={handleDelete}
      />
    </div>


  )
}

export default UserDetailsCard;