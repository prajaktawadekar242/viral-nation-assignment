import * as React from 'react';
import axios from 'axios';
import Button from '@mui/material/Button';
import FormatAlignJustifyIcon from '@mui/icons-material/FormatAlignJustify';
import NightlightRoundIcon from '@mui/icons-material/NightlightRound';
import LightModeIcon from '@mui/icons-material/LightMode';
import ViewListIcon from '@mui/icons-material/ViewList';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { withTheme } from 'styled-components'
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import ToggleOnIcon from '@mui/icons-material/ToggleOn';
import ToggleOffIcon from '@mui/icons-material/ToggleOff';
import PersonAddAlt1Icon from '@mui/icons-material/PersonAddAlt1';
import InputBase from '@mui/material/InputBase';
import { ColorModeContext } from '../common/context';
import UserDetailsCard from './userDetailsCard';
import UserDetailsList from './userDetailsList';
import CreateProfileForm from '../common/CreateProfileForm';
import InfiniteScroll from 'react-infinite-scroll-component';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  width: '100%',
}));

class HomePage extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      page: 0,
      cardCount: 16,
      userProfiles: [],
      showView: true,
      showCreateProfile: false,
      buttonText: '',
      item: {},
      rows: [],
      hasMore: true
    };
  }

  handleClickCardView = () => {
    this.setState({ showView: true })
  }

  handleClickListView = () => {
    this.setState({ showView: false })
  }

  onCreateClickHandler = () => {
    this.setState({ showCreateProfile: true, buttonText: 'Create profile', item: {} })
  }

  setShowCreateProfile = (value) => { this.setState({ showCreateProfile: value }) }

  setButtonText = (value) => { this.setState({ buttonText: value }) }

  setItem = (value) => { this.setState({ item: value }) }

  setUserProfiles = (value) => { this.setState({ userProfiles: value }) }

  onSearchHandler = (value) => {
    /*  const filteredRows = this.state.userProfiles.filter((row) => {
         return row.first_name.toLowerCase().includes(value.target.value.toLowerCase());
       });
     } */
  }

  componentDidMount() {
    this.getUsersData(this.state.page);
  }

  getUsersData = (page) => {
    if (this.state.cardCount === this.state.userProfiles.length) {
      this.setState({ hasMore: false });
      return;
    }
    if (page === 0 || parseInt(this.state.userProfiles.length) / page === 16) {
      const header =
      {
        'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVfbmFtZSI6IkFpdGV5IiwiaXNfY2FuZGlkYXRlIjp0cnVlLCJpYXQiOjE2Nzg5MDUxMTcsImV4cCI6MTY3OTQyMzUxN30.7-kBNeJpJnH1XjIa9vQsd1RZgyx5oaLm3_Qva8bT5W4'

      }
      const body = {
        'query': 'query GetAllProfiles($orderBy: globalOrderBy, $searchString: String, $rows: Int, $page: Int) {\n  getAllProfiles(orderBy: $orderBy, searchString: $searchString, rows: $rows, page: $page) {\n    size\n    profiles {\n      id\n      first_name\n      last_name\n      email\n      is_verified\n      image_url\n      description\n    }\n  }\n}',
        'variables': { 'orderBy': { 'key': 'is_verified', 'sort': 'desc' }, 'rows': 16, 'page': page, 'searchString': '' }
      }
      axios({
        method: 'POST',
        url: 'https://api.poc.graphql.dev.vnplatform.com/graphql',
        headers: header,
        data: body
      })
        .then(response => {
          if (page > 0) {
            const updatedUsers = this.state.userProfiles.concat(response.data.data.getAllProfiles.profiles);
            this.setState({ userProfiles: updatedUsers });
          }
          else {
            this.setState({
              userProfiles: response.data.data.getAllProfiles.profiles,
              cardCount: response.data.data.getAllProfiles.size
            })
          }
          this.setState({ page: this.state.page + 1 })
        }).catch(error => console.log(error))
    }

  }

  static contextType = ColorModeContext;

  render() {
    const getColor = this.props.theme === 'light' ? 'info' : 'inherit'
    return (
      <>
        <ColorModeContext.Consumer>
          {() =>
            <>
              {this.state.showCreateProfile ?
                <CreateProfileForm
                  buttonText={this.state.buttonText} data={this.state.userProfiles} setUserProfiles={this.setUserProfiles}
                  item={this.state.item} setItem={this.setItem} setShowCreateProfile={this.setShowCreateProfile}
                  showCreateProfile={this.state.showCreateProfile} /> : ''}
              <Box sx={{ flexGrow: 1 }}>
                <AppBar position='static' color='transparent'>
                  <Toolbar>
                    <Typography variant='h3'>
                      V
                    </Typography>
                    <Typography variant='h6'>
                      iral Nation
                    </Typography>
                    <Stack direction='row' spacing={1} marginLeft='auto'>
                      <IconButton sx={{ ml: 1 }} onClick={this.context.toggleColorMode} color='inherit'>
                        <LightModeIcon />
                      </IconButton>
                      <IconButton sx={{ ml: 1 }} onClick={this.context.toggleColorMode} color='inherit'>
                        {this.props.theme === 'dark' ? <ToggleOnIcon /> : <ToggleOffIcon />}
                      </IconButton>
                      <IconButton sx={{ ml: 1 }} onClick={this.context.toggleColorMode} color='inherit'>
                        <NightlightRoundIcon />
                      </IconButton>
                    </Stack>
                  </Toolbar>
                </AppBar>
              </Box>
              <Box sx={{
                display: 'flex', flexGrow: 1, textAlign: 'left', marginTop: '2%', maxWidth: '83%',
                marginLeft: '7%'
              }}>


                <Search>
                  <InputBase placeholder='Search' defaultValue={''}
                    sx={{ border: '1px solid #757575', width: '100%', paddingLeft: '2%' }}
                    onChange={value => this.onSearchHandler(value)} >

                  </InputBase>
                </Search>
                <Stack direction='row' marginLeft='1%' >
                  <Button style={{ width: '200px', height: '35px' }}
                    color={getColor} variant='outlined' startIcon={<PersonAddAlt1Icon />}
                    onClick={this.onCreateClickHandler}>Create Profile</Button>
                  <IconButton onClick={this.handleClickCardView} sx={{
                    border: '1px solid #757575',
                    borderRadius: '0%',
                    marginLeft: '5%',
                    height: '35px'
                  }} >
                    <FormatAlignJustifyIcon />
                  </IconButton>
                  <IconButton onClick={this.handleClickListView} sx={{
                    border: '1px solid #757575',
                    borderRadius: '0%',
                    height: '35px'
                  }}>
                    <ViewListIcon />
                  </IconButton>
                </Stack>

              </Box>
              {(this.state.userProfiles && this.state.userProfiles.length > 0) ?

              (this.state.showView ?

                <InfiniteScroll
                  dataLength={this.state.userProfiles.length}
                  next={this.getUsersData(this.state.page)}
                  hasMore={this.state.hasMore}
                >
                  <Grid
                    container
                    spacing={2}
                    direction='row'
                    justify='flex-start'
                    alignItems='flex-start'
                    paddingTop={4}
                    marginLeft='6%'
                    maxWidth='100%'
                  >
                    {this.state.userProfiles.map((item, index) => (

                      <UserDetailsCard
                        cardData={item}
                        item={this.state.item}
                        data={this.state.userProfiles}
                        setUserProfiles={this.setUserProfiles}
                        setShowCreateProfile={this.setShowCreateProfile}
                        setButtonText={this.setButtonText}
                        setItem={this.setItem} />

                    ))}</Grid>
                </InfiniteScroll>
               :

                <UserDetailsList
                  data={this.state.userProfiles}
                  item={this.state.item}
                  setShowCreateProfile={this.setShowCreateProfile}
                  setUserProfiles={this.setUserProfiles}
                  setButtonText={this.setButtonText}
                  setItem={this.setItem} />) : 
                  <h3 style={{marginTop:"25%"}}>Something went wrong...  Please try again later</h3>}
            </>
          }
        </ColorModeContext.Consumer>
      </>
    )
  }
}

export default withTheme(HomePage)