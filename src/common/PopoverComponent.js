import React from 'react';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';

function PopoverComponent(props) {
  return (
    <Popover
      id={props.id}
      open={props.open}
      anchorEl={props.anchorEl}
      onClose={props.handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'left',
      }}
    >
      <Typography sx={{ p: 2, cursor: 'pointer' }} onClick={props.onClickEditHandler}>Edit Profile</Typography>
      <Typography sx={{ p: 2, cursor: 'pointer' }} onClick={props.onClickRemoveHandler}>Remove Profile</Typography>
    </Popover>
  )
}

export default PopoverComponent;

