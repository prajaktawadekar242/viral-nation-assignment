import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

function DialogComponent(props) {
  return (
    <BootstrapDialog
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      maxWidth='xs'
    >
      <BootstrapDialogTitle id="customized-dialog-title" onClose={props.handleClose}>
        Remove profile
      </BootstrapDialogTitle>
      <DialogContent dividers>
        <Typography gutterBottom>
          Removed profile will be deleted permanently and won't be available anymore
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.handleClose} size="small" variant="outlined" color="secondary">Cancel</Button>
        <Button onClick={props.handleDelete} size="small" variant="contained" color="error" autoFocus>
          Delete
        </Button>
      </DialogActions>
    </BootstrapDialog>
  )
}

export default DialogComponent;