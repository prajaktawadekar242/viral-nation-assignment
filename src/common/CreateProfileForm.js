import { useState, React } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { FormControl, FormLabel, IconButton } from '@mui/material';
import Typography from '@mui/material/Typography';
import Switch from '@mui/material/Switch';
import axios from 'axios';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import CloseIcon from '@mui/icons-material/Close';

function CreateProfileForm(props) {

    const [values, setValues] = useState({
        image: props.item ? props.item.image_url : "",
        firstName: props.item ? props.item.first_name : "",
        lastName: props.item ? props.item.last_name : "",
        email: props.item ? props.item.email : "",
        desc: props.item ? props.item.description : ""
    });
    const [checked, setChecked] = useState(true);

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };

    const handleChangeSwitch = (event) => {
        setChecked(event.target.checked);
    };

    const onFormClose = () => {
        props.setShowCreateProfile(false)
        props.setItem({});
    }

    const onButtonClick = () => {
        props.setShowCreateProfile(false)
        props.setItem({});

        if (props.buttonText === "Create profile") {
            const header =
            {
                "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVfbmFtZSI6IkFpdGV5IiwiaXNfY2FuZGlkYXRlIjp0cnVlLCJpYXQiOjE2Nzg5MDUxMTcsImV4cCI6MTY3OTQyMzUxN30.7-kBNeJpJnH1XjIa9vQsd1RZgyx5oaLm3_Qva8bT5W4",
            }
            const body = {
                "query": "mutation CreateProfile($firstName: String!, $lastName: String!, $email: String!, $isVerified: Boolean!, $imageUrl: String!, $description: String!) {\n  createProfile(first_name: $firstName, last_name: $lastName, email: $email, is_verified: $isVerified, image_url: $imageUrl, description: $description) {\n    id\n    first_name\n    last_name\n    email\n    is_verified\n    image_url\n    description\n  }\n}",
                "variables": {
                    "firstName": values.firstName,
                    "lastName": values.lastName,
                    "email": values.email,
                    "isVerified": checked,
                    "imageUrl": values.image,
                    "description": values.desc
                }
            }
            axios({
                method: 'POST',
                url: "https://api.poc.graphql.dev.vnplatform.com/graphql",
                headers: header,
                data: body
            })
                .then(response => props.setUserProfiles(props.data.concat(response.data.data.createProfile)))
                .catch((error) => console.log(error));
        }
        else {
            const header =
            {
                "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVfbmFtZSI6IkFpdGV5IiwiaXNfY2FuZGlkYXRlIjp0cnVlLCJpYXQiOjE2Nzg5MDUxMTcsImV4cCI6MTY3OTQyMzUxN30.7-kBNeJpJnH1XjIa9vQsd1RZgyx5oaLm3_Qva8bT5W4",
            }
            const body = {
                "query": "mutation UpdateProfile($updateProfileId: String!, $firstName: String!, $lastName: String!, $email: String!, $isVerified: Boolean!, $imageUrl: String!, $description: String!) {\n  updateProfile(id: $updateProfileId, first_name: $firstName, last_name: $lastName, email: $email, is_verified: $isVerified, image_url: $imageUrl, description: $description) {\n    id\n    first_name\n    last_name\n    email\n    is_verified\n    image_url\n    description\n  }\n}",
                "variables": {
                    "updateProfileId": props.item.id,
                    "firstName": values.firstName,
                    "lastName": values.lastName,
                    "email": values.email,
                    "isVerified": checked,
                    "imageUrl": values.image,
                    "description": values.desc
                }
            }
            axios({
                method: 'POST',
                url: "https://api.poc.graphql.dev.vnplatform.com/graphql",
                headers: header,
                data: body
            })
                .then(response => {
                    props.setUserProfiles(props.data.map(card => {
                        if (card.id === response.data.data.updateProfile.id) {
                            return response.data.data.updateProfile
                        } else {
                            return card
                        }
                    }));
                })
                .catch((error) => console.log(error));

        }
    }
    const theme = useTheme();
    const color = theme.palette.mode === "dark" ? '#181A1C' : '#FCFCFD';

    return (
        <div style={{
            position: 'absolute', left: '60%', height: '100vh', width: '40%',
            backgroundColor: color,
            zIndex: '1', display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            padding: '0px'
        }}>
            <div style={{ width: "100%", display: 'flex', flexDirection: "row", marginTop: "10%" }}>
                <Typography sx={{ marginLeft: "10%" }}>{props.buttonText}</Typography>
                <IconButton sx={{ marginLeft: "50%" }} onClick={onFormClose}>
                    <CloseIcon />
                </IconButton>
            </div>

            <FormControl sx={{ width: "80%", marginLeft: "10%", alignItems: 'flex-start' }}>
                <FormLabel>Image link</FormLabel>

                <TextField defaultValue={values.image} size="small"
                    onChange={handleChange('image')} ></TextField>
                <div style={{ display: 'flex', flexDirection: "row", width: "100%", marginTop: "2%" }}>
                    <FormLabel >First name</FormLabel>
                    <FormLabel sx={{ marginLeft: "30%" }}>Last name</FormLabel></div>
                <div style={{ display: 'flex', flexDirection: "row", marginBottom: "2%" }}>

                    <TextField defaultValue={values.firstName} size="small"
                        onChange={handleChange('firstName')} ></TextField>

                    <TextField defaultValue={values.lastName} size="small"
                        onChange={handleChange('lastName')} ></TextField>
                </div>
                <FormLabel>Email</FormLabel>

                <TextField sx={{ marginBottom: "2%" }} defaultValue={values.email} size="small"
                    onChange={handleChange('email')} ></TextField>
                <FormLabel>Description</FormLabel>

                <TextField
                    id="outlined-multiline-static"
                    multiline
                    rows={4}
                    placeholder="Write a description for the talent"
                    defaultValue={values.desc}
                    sx={{ marginBottom: "2%" }}
                    onChange={handleChange('desc')}
                />
                <FormLabel sx={{ marginBottom: "2%" }}>Verification</FormLabel>
                <Box sx={{ flexGrow: 1, display: 'flex', textAlign: 'left', maxWidth: '100%' }}>
                    <Typography>Talent is verified</Typography>

                    <Switch sx={{ marginLeft: "30%" }}
                        onChange={handleChangeSwitch} checked={checked} />
                </Box>

                <Button
                    disabled={values.image === "" || values.email === "" || values.firstName === "" || values.lastName === "" || values.desc === ""}
                    onClick={onButtonClick}>{props.buttonText}</Button>
            </FormControl>

        </div>
    )
}

export default CreateProfileForm;