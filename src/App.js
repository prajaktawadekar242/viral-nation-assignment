import './App.css';
import * as React from 'react';
import HomePage from './components/HomePage';
import { ThemeProvider } from "@mui/material";
import { createTheme } from '@mui/material/styles';
import CssBaseline from "@mui/material/CssBaseline";
import ErrorBoundary from './common/ErrorBoundary';
import { ColorModeContext } from './common/context';


function App() {
  const [mode, setMode] = React.useState('dark');
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
      },
    }),
    [],
  );

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode],
  );

  return (
    <div className="App">
     <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
      <CssBaseline />
      <ErrorBoundary >
        <HomePage theme={theme.palette.mode}/>
        </ErrorBoundary>
      </ThemeProvider>
      </ColorModeContext.Provider>
    </div>
  );
}

export default App;
